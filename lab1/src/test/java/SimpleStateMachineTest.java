import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class SimpleStateMachineTest {
    @Test
    public void test1() throws IOException {
        String[] input = {
                "hugo",
        };

        var sutOutputStream = new ByteArrayOutputStream();
        var sut = getSut(input, sutOutputStream);

        assert sut.currentState == SimpleStateMachine.State.S1;
        assertEndsWith("on", sutOutputStream);
    }

    @Test
    public void test2() throws IOException {
        String[] input = {
                "blah",
        };

        var sutOutputStream = new ByteArrayOutputStream();
        var sut = getSut(input, sutOutputStream);

        assert sut.currentState == SimpleStateMachine.State.S0;
    }

    @Test
    public void test3() throws IOException {
        String[] input = {
                "hugo",
                "erna"
        };

        var sutOutputStream = new ByteArrayOutputStream();
        var sut = getSut(input, sutOutputStream);

        assert sut.currentState == SimpleStateMachine.State.S2;
        assertEndsWith("off", sutOutputStream);
    }

    @Test
    public void test4() throws IOException {
        String[] input = {
                "hugo",
                "erna",
                "blah"
        };

        var sutOutputStream = new ByteArrayOutputStream();
        var sut = getSut(input, sutOutputStream);

        assert sut.currentState == SimpleStateMachine.State.S2;
    }

    @Test
    public void test5() throws IOException {
        String[] input = {
                "hugo",
                "erna",
                "alfred"
        };

        var sutOutputStream = new ByteArrayOutputStream();
        var sut = getSut(input, sutOutputStream);

        assert sut.currentState == SimpleStateMachine.State.S1;
        assertEndsWith("", sutOutputStream);
    }

    @Test
    public void test6() throws IOException {
        String[] input = {
                "hugo",
                "erna",
                "",
        };

        var sutOutputStream = new ByteArrayOutputStream();
        var sut = getSut(input, sutOutputStream);

        assert sut.currentState == SimpleStateMachine.State.S0;
        assertEndsWith("Blink", sutOutputStream);
    }

    @Test
    public void test7() throws IOException {
        String[] input = {
                "hugo",
                "erna",
                "blah"
        };

        var sutOutputStream = new ByteArrayOutputStream();
        var sut = getSut(input, sutOutputStream);

        assert sut.currentState == SimpleStateMachine.State.S2;
    }

    private SimpleStateMachine getSut(String[] userInput, OutputStream sutOutputStream) {
        var sut = new SimpleStateMachine();
        var output = new ByteArrayOutputStream();
        var writer = new PrintWriter(output);

        for (String inp: userInput) {
            writer.println(inp);
        }

        writer.flush();

        var inputStream = new ByteArrayInputStream(output.toByteArray());

        var sutPrintStream = new PrintStream(sutOutputStream);

        sut.start(inputStream, sutPrintStream);

        return sut;
    }

    private void assertEndsWith(String end, ByteArrayOutputStream stream) {
        assert stream.toString(StandardCharsets.UTF_8).contains(end + "\n");
    }
}
