import org.junit.Test;

public class ToasterTest {

    @Test
    public void startsInS0() {
        var t = initS0();

        assert t.getState() == Toaster.STATE.S0;
    }

    @Test
    public void s0IgnoresInvalid() {
        var t = initS0();

        t.input("blah");

        assert t.getState() == Toaster.STATE.S0;
    }

    @Test
    public void s0IncToS6() {
        var t = initS0();

        t.input("inc");

        assert t.getState() == Toaster.STATE.S6;
    }

    @Test
    public void s1DecToS7() {
        var t = initS1();

        t.input("inc");

        assert t.getState() == Toaster.STATE.S7;
    }

    @Test
    public void s2DefrostToS0() {
        var t = initS2();

        t.input("defrost");

        assert t.getState() == Toaster.STATE.S0;
    }

    private Toaster initS0() {
        return new Toaster();
    }

    private Toaster initS1() {
        var t = initS0();

        t.input("push");

        return t;
    }

    private Toaster initS2() {
        var t = initS0();

        t.input("defrost");

        return t;
    }

    private Toaster initS3() {
        var t = initS2();

        t.input("push");

        return t;
    }

    private Toaster initS4() {
        var t = initS2();

        t.input("inc");

        return t;
    }

    private Toaster initS5() {
        var t = initS4();

        t.input("push");

        return t;
    }

    private Toaster initS6() {
        var t = initS4();

        t.input("defrost");

        return t;
    }

    private Toaster initS7() {
        var t = initS6();

        t.input("stop");

        return t;
    }
}
