public class Toaster {
    public enum STATE {
        S0,
        S1,
        S2,
        S3,
        S4,
        S5,
        S6,
        S7
    }

    private STATE state = STATE.S0;

    public STATE getState() {
        return state;
    }

    public String input(String input) {
        String output;

        switch (state) {
            case S0:
                output = s0(input);
                break;
            case S1:
                output = s1(input);
                break;
            case S2:
                output = s2(input);
                break;
            case S3:
                output = s3(input);
                break;
            case S4:
                output = s4(input);
                break;
            case S5:
                output = s5(input);
                break;
            case S6:
                output = s6(input);
                break;
            case S7:
                output = s7(input);
                break;
            default:
                output = null;
                break;
        }

        return output;
    }

    private String s0(String input) {
        String output = null;

        switch (input) {
            case "inc":
                state = STATE.S6;
                break;
            case "defrost":
                state = STATE.S2;
                break;
            case "push":
                state = STATE.S1;
                output = "on";
                break;
            default:
                break;
        }

        return output;
    }

    private String s1(String input) {
        String output = null;

        switch (input) {
            case "stop":
            case "time":
                state = STATE.S0;
                output = "off";
                break;
            case "inc":
                state = STATE.S7;
                break;
            default:
                break;
        }

        return output;
    }

    private String s2(String input) {
        String output = null;

        switch (input) {
            case "defrost":
                state = STATE.S0;
                break;
            case "inc":
                state = STATE.S4;
                break;
            case "push":
                state = STATE.S3;
                output = "on";
                break;
            default:
                break;
        }

        return output;
    }

    private String s3(String input) {
        String output = null;

        switch (input) {
            case "time_d":
                state = STATE.S1;
                break;
            case "stop":
                state = STATE.S0;
                output = "off";
                break;
            case "inc":
                state = STATE.S5;
                break;
            default:
                break;
        }

        return output;
    }

    private String s4(String input) {
        String output = null;

        switch (input) {
            case "dec":
                state = STATE.S2;
                break;
            case "defrost":
                state = STATE.S6;
                break;
            case "push":
                state = STATE.S5;
                output = "on";
                break;
            default:
                break;
        }

        return output;
    }

    private String s5(String input) {
        String output = null;

        switch (input) {
            case "dec":
                state = STATE.S3;
                break;
            case "stop":
                state = STATE.S6;
                output = "off";
                break;
            case "time_d":
                state = STATE.S7;
                break;
            default:
                break;
        }

        return output;
    }

    private String s6(String input) {
        String output = null;

        switch (input) {
            case "defrost":
                state = STATE.S4;
                break;
            case "dec":
                state = STATE.S0;
                break;
            case "push":
                state = STATE.S7;
                output = "on";
                break;
        }

        return output;
    }

    private String s7(String input) {
        String output = null;

        switch (input) {
            case "dec":
                state = STATE.S1;
                break;
            case "time":
            case "stop":
                state = STATE.S6;
                output = "off";
                break;
        }

        return output;
    }
}
